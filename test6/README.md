﻿<!-- markdownlint-disable MD033-->
<!-- 禁止MD033类型的警告 https://www.npmjs.com/package/markdownlint -->

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 | [返回](./README.md)

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql [pdborcl_system.sql](./pdborcl_system.sql)。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|

## 实验步骤

### 表及表空间使用方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。

1. 

```sql
-- 创建主表空间
CREATE TABLESPACE sales_data
  DATAFILE 'sales_data.dbf'
  SIZE 100M
  AUTOEXTEND ON
  NEXT 100M
  MAXSIZE UNLIMITED;

-- 创建索引表空间
CREATE TABLESPACE sales_index
  DATAFILE 'sales_index.dbf'
  SIZE 100M
  AUTOEXTEND ON
  NEXT 100M
  MAXSIZE UNLIMITED;
```

![image-20230522214228053](./assets/image-20230522214228053.png)

2. 

```sql
-- 创建商品表
CREATE TABLE Products (
  ProductID NUMBER PRIMARY KEY,
  Name VARCHAR2(100),
  Description VARCHAR2(255),
  Price NUMBER
) TABLESPACE SALES_DATA;

-- 创建客户表
CREATE TABLE Customers (
  CustomerID NUMBER PRIMARY KEY,
  Name VARCHAR2(100),
  Address VARCHAR2(255),
  Contact VARCHAR2(100)
) TABLESPACE SALES_DATA;

-- 创建销售订单表
CREATE TABLE SalesOrders (
  OrderID NUMBER PRIMARY KEY,
  OrderDate DATE,
  CustomerID NUMBER,
  CONSTRAINT fk_customer
    FOREIGN KEY (CustomerID)
    REFERENCES Customers(CustomerID)
) TABLESPACE SALES_DATA;

-- 创建销售订单详情表
CREATE TABLE SalesOrderDetails (
  OrderID NUMBER,
  ProductID NUMBER,
  Quantity NUMBER,
  Price NUMBER,
  CONSTRAINT pk_sales_order_details
    PRIMARY KEY (OrderID, ProductID),
  CONSTRAINT fk_order
    FOREIGN KEY (OrderID)
    REFERENCES SalesOrders(OrderID),
  CONSTRAINT fk_product
    FOREIGN KEY (ProductID)
    REFERENCES Products(ProductID)
) TABLESPACE SALES_DATA;

```

![image-20230522214703646](./assets/image-20230522214703646.png)

3. 

```sql
-- 插入模拟数据
BEGIN
  FOR i IN 1..100000 LOOP
    DECLARE
      v_OrderID NUMBER;
      v_ProductID NUMBER;
      v_CustomerID NUMBER;
      v_Quantity NUMBER;
      v_Price NUMBER;
    BEGIN
      -- 插入商品表数据并获取价格
      v_ProductID := i;
      INSERT INTO Products(ProductID, Name, Description, Price)
      VALUES(v_ProductID, 'Product ' || v_ProductID, 'Description ' || v_ProductID, ROUND(DBMS_RANDOM.VALUE(1, 100), 2));

      -- 插入客户表数据
      v_CustomerID := i;
      INSERT INTO Customers(CustomerID, Name, Address, Contact)
      VALUES(v_CustomerID, 'Customer ' || v_CustomerID, 'Address ' || v_CustomerID, 'Contact ' || v_CustomerID);

      -- 插入销售订单表数据
      v_OrderID := i;
      INSERT INTO SalesOrders(OrderID, OrderDate, CustomerID)
      VALUES(v_OrderID, SYSDATE, v_CustomerID);

      -- 插入销售订单详情表数据并计算价格
      v_Quantity := ROUND(DBMS_RANDOM.VALUE(1, 10));
      SELECT Price INTO v_Price FROM Products WHERE ProductID = v_ProductID;
      v_Price := v_Price * v_Quantity;

      INSERT INTO SalesOrderDetails(OrderID, ProductID, Quantity, Price)
      VALUES(v_OrderID, v_ProductID, v_Quantity, v_Price);
    END;
  END LOOP;
  COMMIT;
END;
/

```

![image-20230522215120288](./assets/image-20230522215120288.png)

### 设计权限及用户分配方案。至少两个用户

```sql
-- 创建SALE_APP_USER用户并分配权限
CREATE USER SALE_APP_USER IDENTIFIED BY password DEFAULT TABLESPACE sales_data;
GRANT CREATE SESSION, CREATE TABLE, CREATE SEQUENCE, CREATE PROCEDURE TO SALE_APP_USER;
GRANT UNLIMITED TABLESPACE TO SALE_APP_USER;

-- 创建SALE_ADMIN_USER用户并分配权限
CREATE USER SALE_ADMIN_USER IDENTIFIED BY password DEFAULT TABLESPACE sales_data;
GRANT DBA TO SALE_ADMIN_USER;
```

![image-20230522215226701](./assets/image-20230522215226701.png)

### 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑

```sql
create or replace PACKAGE SALES_PACKAGE AS
  -- 存储过程：创建销售订单
  PROCEDURE CREATE_SALES_ORDER(
    p_customer_id IN NUMBER,
    p_product_id IN NUMBER,
    p_quantity IN NUMBER
  );

  -- 存储过程：取消销售订单
  PROCEDURE CANCEL_SALES_ORDER(
    p_order_id IN NUMBER
  );


  -- 存储过程：批量取消订单
  PROCEDURE CANCEL_ORDERS_BY_CUSTOMER(
    p_customer_id IN NUMBER
  );
  -- 函数：计算订单总金额
  FUNCTION CALCULATE_ORDER_TOTAL(
    p_order_id IN NUMBER
  ) RETURN NUMBER;

  -- 函数：查询指定商品的销售统计信息
  FUNCTION GET_PRODUCT_SALES_STATS(
    p_product_id IN NUMBER
  ) RETURN SYS_REFCURSOR;

  -- 查询所有商品
  FUNCTION GET_ALL_PRODUCTS RETURN SYS_REFCURSOR;

  -- 查询价格大于等于指定值的商品
  FUNCTION GET_PRODUCTS_BY_PRICE(
    p_min_price IN NUMBER
  ) RETURN SYS_REFCURSOR;

  -- 查询指定订单ID的订单详情
  FUNCTION GET_ORDER_DETAILS_BY_ORDER_ID(
    p_order_id IN NUMBER
  ) RETURN SYS_REFCURSOR;

  -- 查询价格大于等于指定值的订单详情
  FUNCTION GET_ORDER_DETAILS_BY_PRICE(
    p_min_price IN NUMBER
  ) RETURN SYS_REFCURSOR;

  -- 查询消费最高的用户
  FUNCTION GET_TOP_SPENDING_CUSTOMER RETURN SYS_REFCURSOR;

  -- 查询消费的中位数
  FUNCTION GET_SPENDING_MEDIAN RETURN NUMBER;

  -- 查询消费最频繁的金额和次数
  FUNCTION GET_TOP_SPENDING_FREQUENCY RETURN SYS_REFCURSOR;
END SALES_PACKAGE;

```

![image-20230522215343395](./assets/image-20230522215343395.png)

调用函数，存储过程和查询

```sql
-- 查询消费最高的用户
SET SERVEROUTPUT ON;
DECLARE
  cur SYS_REFCURSOR;
  v_customer_id NUMBER;
  v_customer_name VARCHAR2(100);
  v_total_spent NUMBER;
BEGIN
  cur := SALES_PACKAGE.GET_TOP_SPENDING_CUSTOMER;

  -- 从游标中获取结果
  FETCH cur INTO v_customer_id, v_customer_name, v_total_spent;

  -- 检查是否成功获取结果
  IF cur%FOUND THEN
    DBMS_OUTPUT.PUT_LINE('最高消费用户：');
    DBMS_OUTPUT.PUT_LINE('用户ID: ' || v_customer_id);
    DBMS_OUTPUT.PUT_LINE('用户姓名: ' || v_customer_name);
    DBMS_OUTPUT.PUT_LINE('消费总额: ' || v_total_spent);
  ELSE
    DBMS_OUTPUT.PUT_LINE('没有找到消费最高的用户。');
  END IF;

  CLOSE cur;
EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('错误: ' || SQLCODE || ' - ' || SQLERRM);
END;

```

![image-20230522215803468](./assets/image-20230522215803468.png)

```sql
-- 查询价格大于等于指定值的订单详情
DECLARE
  cur SYS_REFCURSOR;
  v_min_price NUMBER := &min_price; -- 输入最低价格
  v_product_id NUMBER;
  v_product_name VARCHAR2(100);
  v_product_price NUMBER;
BEGIN
  cur := SALES_PACKAGE.GET_PRODUCTS_BY_PRICE(v_min_price);
  LOOP
    FETCH cur INTO v_product_id, v_product_name, v_product_price;
    EXIT WHEN cur%NOTFOUND;
    DBMS_OUTPUT.PUT_LINE('商品ID: ' || v_product_id || ', 商品名称: ' || v_product_name || ', 商品价格: ' || v_product_price);
  END LOOP;
  CLOSE cur;
  DBMS_OUTPUT.PUT_LINE('查询完成。');
EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('错误: ' || SQLCODE || ' - ' || SQLERRM);
END;

```

![image-20230522220029174](./assets/image-20230522220029174.png)

```sql
-- 查询所有商品
DECLARE
  cur SYS_REFCURSOR;
BEGIN
  cur := SALES_PACKAGE.GET_ALL_PRODUCTS;
  -- 使用游标进行操作，例如循环遍历结果集
  -- ...
  DBMS_OUTPUT.PUT_LINE('查询完成。');
EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('错误: ' || SQLCODE || ' - ' || SQLERRM);
END;

```

![image-20230522220144766](./assets/image-20230522220144766.png)

```sql
-- 根据用户id批量取消订单
SET SERVEROUTPUT ON;
DECLARE
  v_order_id NUMBER;
BEGIN
  -- 从用户获取订单ID
  v_order_id := &p_order_id; -- 输入订单ID

  SALES_PACKAGE.CANCEL_SALES_ORDER(v_order_id);
  DBMS_OUTPUT.PUT_LINE('订单已取消。');
EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('错误: ' || SQLCODE || ' - ' || SQLERRM);
END;

```

![image-20230522220219051](./assets/image-20230522220219051.png)

### 设计一套数据库的备份方案

1. 使用下面命令以 `sysdba`登录数据库

```sql
sqlplus / as sysdba
```

![image-20230522220541252](./assets/image-20230522220541252.png)

2. 关闭数据库并以 `MOUNT` 模式重新启动

```sql
SHUTDOWN IMMEDIATE
STARTUP MOUNT
```

![image-20230522220804503](./assets/image-20230522220804503.png)

3. 切换到归档模式

```sql
ALTER DATABASE ARCHIVELOG;
```

![image-20230522220824462](./assets/image-20230522220824462.png)

4. 打开数据库

```sql
ALTER DATABASE OPEN;
```

![image-20230522220840396](./assets/image-20230522220840396.png)

5. 重新打开一个终端，使用下面命令连接数据库

```sh
rman target /
```

6. 显示配置

```sql
SHOW ALL;
```

![image-20230522221006024](./assets/image-20230522221006024.png)

7. 备份整个数据库

```sql
BACKUP DATABASE;
```

![image-20230522221114600](./assets/image-20230522221114600.png)

8. 查看备份信息

```sql
LIST BACKUP;
```

![image-20230522221134088](./assets/image-20230522221134088.png)

以上就是本次实验的全部内容了。
