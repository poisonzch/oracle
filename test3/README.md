# 实验3：创建分区表

## 实验目的

掌握分区表的创建方法，掌握各种分区方式的使用场景。

## 实验内容

- 本实验使用实验2的sale用户创建两张表：订单表(orders)与订单详表(order_details)。
- 两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。
- 新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。
- orders表按订单日期（order_date）设置范围分区。
- order_details表设置引用分区。
- 表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）。
- 写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。
- 进行分区与不分区的对比实验。

## 实验参考

- 使用sql-developer软件创建表，并导出类似以下的脚本。
- 以下脚本不含orders.customer_name的索引，不含序列设置，仅供参考。

```sql
CREATE TABLE orders 
(
 order_id NUMBER(9, 0) NOT NULL
 , customer_name VARCHAR2(40 BYTE) NOT NULL 
 , customer_tel VARCHAR2(40 BYTE) NOT NULL 
 , order_date DATE NOT NULL 
 , employee_id NUMBER(6, 0) NOT NULL 
 , discount NUMBER(8, 2) DEFAULT 0 
 , trade_receivable NUMBER(8, 2) DEFAULT 0 
 , CONSTRAINT ORDERS_PK PRIMARY KEY 
  (
    ORDER_ID 
  )
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE (   BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL 

PARTITION BY RANGE (order_date) 
(
 PARTITION PARTITION_BEFORE_2016 VALUES LESS THAN (
 TO_DATE(' 2016-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
 'NLS_CALENDAR=GREGORIAN')) 
 NOLOGGING
 TABLESPACE USERS
 PCTFREE 10 
 INITRANS 1 
 STORAGE 
( 
 INITIAL 8388608 
 NEXT 1048576 
 MINEXTENTS 1 
 MAXEXTENTS UNLIMITED 
 BUFFER_POOL DEFAULT 
) 
NOCOMPRESS NO INMEMORY  
, PARTITION PARTITION_BEFORE_2020 VALUES LESS THAN (
TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING
TABLESPACE USERS
, PARTITION PARTITION_BEFORE_2021 VALUES LESS THAN (
TO_DATE(' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING 
TABLESPACE USERS
);
--以后再逐年增加新年份的分区
ALTER TABLE orders ADD PARTITION partition_before_2022
VALUES LESS THAN(TO_DATE('2022-01-01','YYYY-MM-DD'))
TABLESPACE USERS;

```

- 创建order_details表的语句如下：

```sql
CREATE TABLE order_details
(
id NUMBER(9, 0) NOT NULL 
, order_id NUMBER(10, 0) NOT NULL
, product_id VARCHAR2(40 BYTE) NOT NULL 
, product_num NUMBER(8, 2) NOT NULL 
, product_price NUMBER(8, 2) NOT NULL 
, CONSTRAINT ORDER_DETAILS_PK PRIMARY KEY 
  (
    id 
  )
, CONSTRAINT order_details_fk1 FOREIGN KEY  (order_id)
REFERENCES orders  (  order_id   )
ENABLE
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE ( BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL
PARTITION BY REFERENCE (order_details_fk1);
```

- 创建序列SEQ1的语句如下

```sql
CREATE SEQUENCE  SEQ1  MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
```

- 插入100条orders记录的样例脚本如下：

```sql
declare 
   i integer;
   y integer;
   m integer;
   d integer;
   str varchar2(100);
BEGIN  
  i:=0;
  y:=2015;
  m:=1;
  d:=12;
  while i<100 loop
    i := i+1;
    --在这里改变y,m,d
    m:=m+1;
    if m>12 then
        m:=1;
    end if;
    str:=y||'-'||m||'-'||d;
    insert into orders(order_id,order_date) 
      values(SEQ1.nextval,to_date(str,'yyyy-MM-dd'));
  end loop;
  commit;
END;
/

说明：
|| 表示字符连接符号
SEQ1是一个序列对象
```

## 实验注意事项

- 完成时间：2023-05-08，请按时完成实验，过时扣分。
- 查询语句及分析文档`必须提交`到：你的oracle项目中的test3目录中。
- 实验分析及结果文档说明书用Markdown格式编写。



## 实验步骤

1. 创建两张表，`orders` 和 `order_details`。其中，这两张表通过 `order_id` 列建立主外键关联。

   ```sql
   -- 创建 orders 表
   CREATE TABLE sale.orders (
       order_id NUMBER(10) NOT NULL,
       customer_name VARCHAR2(100) NOT NULL,
       order_date DATE NOT NULL,
       total_price NUMBER(10, 2),
       CONSTRAINT pk_orders PRIMARY KEY (order_id)
   );
   
   -- 创建 order_details 表
   CREATE TABLE sale.order_details (
       id NUMBER(10) NOT NULL,
       order_id NUMBER(10) NOT NULL,
       product_name VARCHAR2(100) NOT NULL,
       product_price NUMBER(10, 2) NOT NULL,
       quantity NUMBER(10) NOT NULL,
       CONSTRAINT pk_order_details PRIMARY KEY (id),
       CONSTRAINT fk_order_details_orders FOREIGN KEY (order_id) REFERENCES sale.orders (order_id)
   );
   
   ```

   ![image-20230506085328152](./assets/image-20230506085328152.png)

2. 创建索引和序列，我们需要给 `orders` 表的 `customer_name` 列添加一个 B-Tree 索引，并创建两个序列：`orders.order_id` 和 `order_details.id`，以便在插入数据时不需要手动设置这两个 ID 值。

```sql
-- 为 orders 表的 customer_name 列创建 B-Tree 索引
CREATE INDEX idx_orders_customer_name ON sale.orders (customer_name) TABLESPACE users;

-- 创建 orders.order_id 的序列
CREATE SEQUENCE sale.seq_order_id START WITH 1 INCREMENT BY 1;

-- 创建 order_details.id 的序列
CREATE SEQUENCE sale.seq_order_details_id START WITH 1 INCREMENT BY 1;

```

![image-20230506085537779](./assets/image-20230506085537779.png)

3. 创建分区表，我们需要为 `orders` 表和 `order_details` 表分别创建分区表。这里，我们使用范围分区和引用分区两种方式。

   1. `orders`表按订单日期范围分区，为orders表创建按订单日期范围的分区表，分区键为order_date，按月分区，分区名格式为`p_yyyymm`：

   ```sql
   -- 创建分区表
   CREATE TABLE orders_range_partitioned
   (
     order_id      NUMBER(10) not null,
     customer_name VARCHAR2(100) not null,
     order_date    DATE not null,
     total_amount  NUMBER(10, 2) not null,
     constraint pk_orders_range_partitioned primary key (order_id)
   )
   PARTITION BY RANGE (order_date)
   INTERVAL(NUMTOYMINTERVAL(1, 'MONTH'))
   (
     PARTITION p_default VALUES LESS THAN (TO_DATE('2023-05-01', 'YYYY-MM-DD'))
   );
   ```

   ![image-20230506090828589](./assets/image-20230506090828589.png)

   

   	2. `order_details`表引用分区，为`order_details`表创建引用分区，参考orders表的分区，分区键为`order_id`：

```sql
-- 创建分区表
CREATE TABLE order_details_ref_partitioned
(
  id          NUMBER(10) not null,
  order_id    NUMBER(10) not null,
  product_name  VARCHAR2(100) not null,
  product_price  NUMBER(10, 2) not null,
  quantity    NUMBER(10) not null,
  constraint pk_order_details_ref_partitioned primary key (id),
  constraint fk_order_details_ref_partitioned_orders foreign key (order_id) references orders_range_partitioned(order_id)
)
PARTITION BY REFERENCE (fk_order_details_ref_partitioned_orders);
```

![image-20230506091232059](./assets/image-20230506091232059.png)

4. 插入数据，插入数据前，需要先禁用约束和索引以提高插入性能：

```sql
-- 禁用orders表约束和索引
ALTER TABLE orders_range_partitioned DISABLE CONSTRAINT pk_orders_range_partitioned;
ALTER INDEX idx_orders_customer_name INVISIBLE;

-- 禁用order_details表约束
ALTER TABLE order_details_ref_partitioned DISABLE CONSTRAINT pk_order_details_ref_partitioned;

```

使用`INSERT INTO SELECT`语句插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）：

```sql
-- 插入orders数据
INSERT INTO orders_range_partitioned (order_id, customer_name, order_date, total_amount)
SELECT sale.seq_order_id.NEXTVAL,
       'customer' || TO_CHAR(LEVEL, 'FM000000'),
       TO_DATE('2021-01-01', 'YYYY-MM-DD') + TRUNC(DBMS_RANDOM.VALUE(0, 365*3)),
       TRUNC(DBMS_RANDOM.VALUE(100, 10000), 2)
FROM DUAL
CONNECT BY LEVEL <= 400000;

-- 插入order_details数据
INSERT INTO order_details_ref_partitioned (id, order_id, product_name, product_price, quantity)
SELECT sale.seq_order_details_id.NEXTVAL,
       order_id,
       'product' || TO_CHAR(LEVEL, 'FM000000'),
       TRUNC(DBMS_RANDOM.VALUE(10, 100), 2),
       TRUNC(DBMS_RANDOM.VALUE(1, 10))
FROM orders_range_partitioned
WHERE order_id <= 400000
CONNECT BY LEVEL <= 5;
```

![image-20230506095006896](./assets/image-20230506095006896.png)

![image-20230506175852096](./assets/image-20230506175852096.png)

插入数据后，需要启用约束和索引：

```sql
-- 启用orders表约束和索引
ALTER INDEX idx_orders_customer_name VISIBLE;
ALTER TABLE orders_range_partitioned ENABLE CONSTRAINT pk_orders_range_partitioned;

-- 启用order_details表约束
ALTER TABLE order_details_ref_partitioned ENABLE CONSTRAINT pk_order_details_ref_partitioned;
```

![image-20230506105026160](./assets/image-20230506105026160.png)

5. 联合查询

```sql
SELECT o.order_id, o.customer_name, o.order_date, o.total_amount,
       d.id, d.product_name, d.quantity, d.product_price
FROM orders_range_partitioned o
JOIN order_details_ref_partitioned d ON o.order_id = d.order_id
WHERE o.order_date BETWEEN TO_DATE('2022-01-01', 'YYYY-MM-DD') AND TO_DATE('2022-12-31', 'YYYY-MM-DD')
ORDER BY o.order_id, d.id;
```

![image-20230506180003540](./assets/image-20230506180003540.png)

6. 执行计划

```sql
EXPLAIN PLAN FOR
SELECT o.order_id, o.customer_name, o.order_date, o.total_amount,
       d.id, d.product_name, d.quantity, d.product_price
FROM orders_range_partitioned o
JOIN order_details_ref_partitioned d ON o.order_id = d.order_id
WHERE o.order_date BETWEEN TO_DATE('2022-01-01', 'YYYY-MM-DD') AND TO_DATE('2022-12-31', 'YYYY-MM-DD')
ORDER BY o.order_id, d.id;

SELECT *
FROM TABLE(DBMS_XPLAN.DISPLAY);
```

![image-20230506180318997](./assets/image-20230506180318997.png) 

7. 分区与不分区对比实验

```sql
-- 创建未分区的orders表
CREATE TABLE orders (
    order_id      NUMBER(10),
    customer_name VARCHAR2(50),
    order_date    DATE,
    total_amount  NUMBER(10, 2),
    CONSTRAINT pk_orders PRIMARY KEY (order_id)
);

-- 创建未分区的order_details表
CREATE TABLE order_details (
    id          NUMBER(10) not null,
    order_id    NUMBER(10) not null,
    product_name  VARCHAR2(100) not null,
    product_price  NUMBER(10, 2) not null,
    quantity    NUMBER(10) not null,
    CONSTRAINT pk_order_details PRIMARY KEY (id),
    CONSTRAINT fk_order_details_orders FOREIGN KEY (order_id) REFERENCES orders (order_id)
);
```



![image-20230506180830454](./assets/image-20230506180830454.png)

```sql
-- 再执行同样的插入和查询语句进行对比

-- 插入orders数据
INSERT INTO orders (order_id, customer_name, order_date, total_amount)
SELECT sale.seq_order_id.NEXTVAL,
       'customer' || TO_CHAR(LEVEL, 'FM000000'),
       TO_DATE('2021-01-01', 'YYYY-MM-DD') + TRUNC(DBMS_RANDOM.VALUE(0, 365*3)),
       TRUNC(DBMS_RANDOM.VALUE(100, 10000), 2)
FROM DUAL
CONNECT BY LEVEL <= 400000;

-- 插入order_details数据
INSERT INTO order_details (id, order_id, product_name, product_price, quantity)
SELECT sale.seq_order_details_id.NEXTVAL,
       order_id,
       'product' || TO_CHAR(LEVEL, 'FM000000'),
       TRUNC(DBMS_RANDOM.VALUE(10, 100), 2),
       TRUNC(DBMS_RANDOM.VALUE(1, 10))
FROM orders_range_partitioned
WHERE order_id <= 400000
CONNECT BY LEVEL <= 5;
```

![image-20230506181118188](./assets/image-20230506181118188.png)

可以看到，执行插入语句时，有没有分区表这个耗时区别是很大的。

## 实验总结

本实验主要介绍了分区表的创建方法和使用场景，并且演示了如何使用范围分区和引用分区两种方式进行分区。通过本实验，我们可以了解到，分区表可以大大提高数据查询和维护的效率，特别是对于大型数据表来说，可以有效地减少数据查询和维护的时间。在实际应用中，我们应该根据具体的业务需求和数据规模来选择合适的分区方式和分区键，从而达到最佳的查询和维护效率。
