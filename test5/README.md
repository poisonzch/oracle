# 实验5：包，过程，函数的用法

## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

1. 创建一个包(Package)，包名是MyPack。
2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
Oracle递归查询的语句格式是：

```sql
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
```

## 实验注意事项，完成时间： 2023-05-16日前上交

- 请按时完成实验，过时扣分。
- 查询语句及分析文档`必须提交`到：你的Oracle项目中的test5目录中。
- 上交后，通过这个地址应该可以打开你的源码：https://github.com/你的用户名/oracle/tree/master/test5
- 实验分析及结果文档说明书用Markdown格式编写。

## 脚本代码参考

```sql
create or replace PACKAGE MyPack IS
/*
本实验以实验4为基础。
包MyPack中有：
一个函数:Get_SalaryAmount(V_DEPARTMENT_ID NUMBER)，
一个过程:Get_Employees(V_EMPLOYEE_ID NUMBER)
*/
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;
/
create or replace PACKAGE BODY MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
AS
    N NUMBER(20,2); --注意，订单ORDERS.TRADE_RECEIVABLE的类型是NUMBER(8,2),汇总之后，数据要大得多。
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
    END;

PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
AS
    LEFTSPACE VARCHAR(2000);
    begin
    --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
    END;
END MyPack;
/
```

## 测试

```sh

函数Get_SalaryAmount()测试方法：
select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;

输出：
DEPARTMENT_ID DEPARTMENT_NAME                SALARY_TOTAL
------------- ------------------------------ ------------
           10 Administration                 4848
           20 Marketing                      19896
           30 Purchasing                     27588
           40 Human Resources                6948
           50 Shipping                       176560

过程Get_Employees()测试代码：
set serveroutput on
DECLARE
V_EMPLOYEE_ID NUMBER;    
BEGIN
V_EMPLOYEE_ID := 101;
MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
END;
/

输出：
101 Neena
    108 Nancy
        109 Daniel
        110 John
        111 Ismael
        112 Jose Manuel
        113 Luis
    200 Jennifer
    203 Susan
    204 Hermann
    205 Shelley
        206 William
```

## 实验步骤

1. 创建一个名为`emp_info`的表用于存储员工信息，包括员工ID，员工姓名，员工职位，员工部门，员工入职日期，员工工资，以及员工经理ID。

```sql
CREATE TABLE emp_info (
  emp_id NUMBER(6) PRIMARY KEY,
  emp_name VARCHAR2(50),
  job_title VARCHAR2(50),
  department VARCHAR2(50),
  hire_date DATE,
  salary NUMBER(10,2),
  manager_id NUMBER(6)
);
```

![image-20230509092638589](./assets/image-20230509092638589.png)

2. 向`emp_info`表中插入数据。

```sql
INSERT INTO emp_info VALUES (100,'Steven','President','Executive',TO_DATE('1987-06-17','YYYY-MM-DD'),24000,NULL);
INSERT INTO emp_info VALUES (101,'Neena','Vice President','Executive',TO_DATE('1989-09-21','YYYY-MM-DD'),17000,100);
INSERT INTO emp_info VALUES (102,'Lex','Director','Executive',TO_DATE('1993-01-13','YYYY-MM-DD'),17000,100);
INSERT INTO emp_info VALUES (103,'Alexander','IT Manager','IT',TO_DATE('1992-01-13','YYYY-MM-DD'),9000,102);
INSERT INTO emp_info VALUES (104,'Bruce','IT Support Engineer','IT',TO_DATE('1993-05-21','YYYY-MM-DD'),6000,103);
INSERT INTO emp_info VALUES (105,'David','Marketing Manager','Marketing',TO_DATE('1994-02-20','YYYY-MM-DD'),13000,100);
INSERT INTO emp_info VALUES (106,'Valli','Marketing Coordinator','Marketing',TO_DATE('1994-02-05','YYYY-MM-DD'),8000,105);
INSERT INTO emp_info VALUES (107,'Diana','Human Resources Manager','Human Resources',TO_DATE('1998-04-20','YYYY-MM-DD'),12000,100);
INSERT INTO emp_info VALUES (108,'Nancy','Accounting Manager','Accounting',TO_DATE('1997-05-21','YYYY-MM-DD'),10500,100);
INSERT INTO emp_info VALUES (109,'Daniel','Sales Manager','Sales',TO_DATE('1996-08-16','YYYY-MM-DD'),11000,100);
INSERT INTO emp_info VALUES (110,'John','Sales Representative','Sales',TO_DATE('1996-12-01','YYYY-MM-DD'),7000,109);
INSERT INTO emp_info VALUES (111,'Ismael','Sales Representative','Sales',TO_DATE('1997-05-30','YYYY-MM-DD'),6200,109);
```

![image-20230509092733922](./assets/image-20230509092733922.png)

3. 创建一个包(Package)，包名是`MyPack`。

```sql
CREATE OR REPLACE PACKAGE MyPack IS

END MyPack;
/
```

![image-20230509092840338](./assets/image-20230509092840338.png)

4. 在`MyPack`中创建一个函数`Get_SalaryAmount`，输入的参数是部门ID，通过查询`emp_info`表，统计每个部门的salary工资总额。

```sql
CREATE OR REPLACE PACKAGE MyPack IS
  FUNCTION Get_SalaryAmount(manager_id IN NUMBER) RETURN NUMBER;
END MyPack;
/

CREATE OR REPLACE PACKAGE BODY MyPack IS
  FUNCTION Get_SalaryAmount(manager_id IN NUMBER) RETURN NUMBER IS
    total_salary NUMBER(10,2);
  BEGIN
    SELECT SUM(salary) INTO total_salary FROM emp_info WHERE department = manager_id;
    RETURN total_salary;
  END Get_SalaryAmount;
END MyPack;
/
```

![image-20230509093014464](./assets/image-20230509093014464.png)

![image-20230509093045426](./assets/image-20230509093045426.png)

5. 在`MyPack`包中创建一个过程`GET_EMPLOYEES`，输入参数是员工ID，在过程中使用游标，通过查询`emp_info`表，递归查询某个员工及其所有下属，子下属员工。

```sql
CREATE OR REPLACE PACKAGE MyPack IS
  FUNCTION Get_SalaryAmount(manager_id IN NUMBER) RETURN NUMBER;
  PROCEDURE GET_EMPLOYEES(emp_id IN NUMBER);
END MyPack;

CREATE OR REPLACE PACKAGE BODY MyPack IS
  FUNCTION Get_SalaryAmount(manager_id IN NUMBER) RETURN NUMBER IS
    total_salary NUMBER(10,2);
  BEGIN
    SELECT SUM(salary) INTO total_salary FROM emp_info WHERE department = manager_id;
    RETURN total_salary;
  END Get_SalaryAmount;

  PROCEDURE GET_EMPLOYEES(emp_id IN NUMBER) IS
    CURSOR employee_cur IS
      SELECT LEVEL, emp_id, emp_name, manager_id FROM emp_info
      START WITH emp_id = emp_id
      CONNECT BY PRIOR emp_id = manager_id;
    emp_rec emp_cur%ROWTYPE;
  BEGIN
    OPEN employee_cur;
    LOOP
      FETCH employee_cur INTO emp_rec;
      EXIT WHEN employee_cur%NOTFOUND;
      DBMS_OUTPUT.PUT_LINE(emp_rec.level || ' ' || emp_rec.emp_id || ' ' || emp_rec.emp_name);
    END LOOP;
    CLOSE employee_cur;
  END GET_EMPLOYEES;
END MyPack;
/
```

![image-20230509093228813](./assets/image-20230509093228813.png)

![image-20230515202432344](./assets/image-20230515202432344.png)

6. 执行查询

```sql
select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;
```



![image-20230515202539690](./assets/image-20230515202539690.png)

```sql
set serveroutput on
DECLARE
V_EMPLOYEE_ID NUMBER;    
BEGIN
V_EMPLOYEE_ID := 101;
MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
END;
/
```

![image-20230515202645615](./assets/image-20230515202645615.png)

## 实验总结

存储过程(Stored Procedure)是一组为了完成特定功能的`SQL`语句集，它大大提高了`SQL`语句的功能和灵活性。存储过程编译后存储在数据库中，所以执行存储过程比执行存储过程中封装的`SQL`语句更有效率。

包用于组合逻辑相关的过程和函数，它由包规范和包体两个部分组成。包规范用于定义公用的常量、变量、过程和函数，创建包规范可以使用`CREATE PACKAGE`命令，创建包体可以使用`CREATE PACKAGE BODY`。

存储过程和函数本质上没区别。只是函数有限制只能返回一个标量，而存储过程可以返回多个。并且函数是可以嵌入在`SQL`中使用的，可以在`SELECT`等`SQL`语句中调用，而存储过程不行。执行的本质都一样，也可以理解函数是存储过程的一种。而包里面有多个存储、函数，当某类存储，或者函数，都是为某一项目的设计的，可以把它们放在一个包里面，这样便于管理。
